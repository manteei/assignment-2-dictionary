global exit
global string_length
global print_error
global print_string
global print_newline
global print_chat
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text

%define ZERO_CODE 0


exit:
	mov rax, 60			
    	syscall


string_length:
	xor rax, rax			
.loop:
    	cmp byte[rdi + rax], ZERO_CODE		
    	jne .next			
    	ret				
.next:
    	inc rax			
    	jmp .loop			
    	

print_error:
	mov r8, 2			
	jmp print_str			
    	

print_string:
	mov r8, 1			


print_str:
	push rdi			
    	call string_length		
    	pop rdi			
    	mov rsi, rdi			
    	mov rdx, rax			
    	mov rdi, r8			
    	mov rax, 1			
    	syscall
    	mov rax, ZERO_CODE			
   	ret
   	

print_newline:
	mov rdi, 0xA			


print_char:
    	mov rax, 1			
    	mov rdx, 1			
    	push rdi			
    	mov rdi, 1			
    	mov rsi, rsp			
    	syscall
    	pop rdi			
    	ret


print_int:
    	cmp rdi, ZERO_CODE			
    	jge print_uint			
    	push rdi			
    	mov rdi, '-'			
    	call print_char		
    	pop rdi			
    	neg rdi			


print_uint:
	mov r9, rsp			
	mov r10, 10			
	mov rax, rdi			
	push ZERO_CODE				
.loop:
	mov rdx, ZERO_CODE		
	div r10			
	add rdx, 0x30			
	dec rsp			
	mov byte[rsp], dl		
	cmp rax, ZERO_CODE			
	jnz .loop
	
	mov rdi, rsp			
	push r9			
	call print_string		
	pop r9				
	mov rsp, r9			
    	ret


string_equals:
    	mov rax, ZERO_CODE			
	call string_length		
    	mov r10, rax		
    	push rdi			
    	mov rdi, rsi			
    	pop rsi			
    	call string_length		
    	cmp rax, r10		
    	jne .noteq			
.loop:
	mov rax, ZERO_CODE			
	mov al, byte[rsi]		
	cmp al, byte[rdi]	
	jne .noteq		
	cmp al, ZERO_CODE			
	je .eq				
	inc rdi		
	inc rsi		
	jmp .loop			
.noteq:
	mov rax, ZERO_CODE			
    	ret
.eq:	
	mov rax, 1			
	ret  	


read_char:
	push ZERO_CODE				
	mov rax, ZERO_CODE			
    	mov rdx, 1			
    	mov rdi, ZERO_CODE			
    	mov rsi, rsp			
    	syscall
    	pop rax			
    	ret 


read_word:
	push r12
	push r13
	push r14			
	mov r12, rdi			
	mov r13, rsi			
	dec r13			
	mov r14, ZERO_CODE			
.check_first:
	call read_char			
	cmp rax, 0x0			
	je .exit			
	cmp rax, 0x20			
	je .check_first		
	cmp rax, 0x9			
	je .check_first		
	cmp rax, 0xA			
	je .check_first		
.loop:
	cmp rax, 0x0			
	je .exit			
	cmp rax, 0x20			
	je .exit			
	cmp rax, 0x9			
	je .exit			
	cmp rax, 0xA			
	je .exit			
	mov [r12+r14], al	
	inc r14			
	cmp r14, r13			
	jg .fail			
	call read_char			
	jmp .loop			
.fail:
	pop r14
	pop r13
	pop r12
	mov rax, ZERO_CODE
	ret
.exit:
	mov byte[r12+r14], ZERO_CODE			
	mov rax, r12			
	mov rdx, r14			
	pop r14				
	pop r13
	pop r12			
	ret


parse_uint:
    	mov rax, ZERO_CODE			
    	mov r10, 10			
    	mov r9, ZERO_CODE			
.loop:
	mov r8, ZERO_CODE			
	mov r8b, byte[rdi + r9]
	cmp r8b, 0x30		
	jb .exit			
	cmp r8b, 0x39		
	ja .exit		
	sub r8b, 0x30	
	mul r10			
	add rax, r8		
	inc r9			
	jmp .loop	
.exit:
	mov rdx, r9			
    	ret



parse_int:
    	cmp byte[rdi], '-'		
    	jne parse_uint			
    	inc rdi			
    	call parse_uint		
    	inc rdx			
    	neg rax			
    	ret


string_copy:
	push rdi
	push rsi
	push rdx			
	call string_length		
	inc rax			
	pop rdx
	pop rsi
	pop rdi			
	cmp rax, rdx		
	jg .fail		
.loop:
	mov r8, ZERO_CODE		
	mov r8b, byte[rdi]	
	mov byte[rsi], r8b	
	inc rdi		
	inc rsi			
	cmp r8b, ZERO_CODE		
	jne .loop			
	ret
.fail:
	xor rax, rax			
	ret
