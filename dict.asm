global find_word
extern string_equals

section .text

find_word:
	xor rax, rax		
.loop:
	cmp rsi, 0		
	je .no_find		
	push rdi		
	push rsi
	add rsi, 8		
	call string_equals	
	pop rsi
	pop rdi
	cmp rax, 1		
	je .find		
	mov rsi, [rsi]		
	jmp .loop		
.no_find:
	xor rax, rax		
	ret
.find:
	mov rax, rsi		
	ret
