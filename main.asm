%include "lib.inc"
%include "dict.inc"
%include "colon.inc"
%include "words.inc"

global _start

%define ZERO_CODE 0
%define BUFFER_SIZE 256
%define DATA_SHIFT 8


section .rodata
notFoundErr: db "Input string not found:(", 0
overErr: db "Input is too long!", 0

section .text

_start:
	sub rsp, BUFFER_SIZE		
	mov rdi, rsp		
	mov rsi, BUFFER_SIZE		
	call read_word		
	cmp rax, ZERO_CODE		
	je .overflow		
	mov rdi, rax	
	mov rsi, ARG	
	call find_word	
	cmp rax, ZERO_CODE	
	je .not_found	
	add rax, DATA_SHIFT		
	push rax	
	mov rdi, rax		
	call string_length	
	inc rax	
	pop rdi	
	add rdi, rax	
	call print_string
	jmp .finish		
.not_found:
	mov rdi, notFoundErr	
	call print_error	
	jmp .finish		
.overflow:
	mov rdi, overErr	
	call print_error	
.finish:
	call print_newline	
	add rsp, BUFFER_SIZE		
	xor rax, rax		
	call exit		
